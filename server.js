const express = require("express");
const app = express();
const port = 3000; // port
const birds = require("./birds"); // ใช้งาน router module
const config = require("config"); // เรียกใช้งานและกําหนด Instance ของโมดูล config
const PORT = config.get("port");

const memberRoute = require('./route/Member')
const authenRoute = require('./route/Authen')

app.use(express.json())
app.use('/authen',authenRoute)
app.use('/member',memberRoute)

app.get("/", (req, res) => {
  res.json({
    success: true,
  });
});

app.listen(PORT, function () {
  console.log(`Server is runnung on port ${PORT}!`);
});

app.listen(5000, () => {
  console.log("API now listening on port 5000");
});
// ดักจับทุกๆ method
app.all("/", function (req, res, next) {
  console.log("Accessing the secret section ...");
  res.myobj = 1; // สมมติเราเพิ่ม req property ที่ชื่อ myobj เท่ากับ 1
  next(); // ให้ไปท างานต่อ handler ฟังก์ชันในล าดับถัดไป
});

// เรียกใช้งานในรูปแบบ middlewar โดยใช้ use
app.use("/birds", birds);
// ส่งกลับข้อความ "hello world" เมื่อมี GET request มายังหน้า homepage

app.get("/", function (req, res) {
  console.log(res.myobj); // ทดสอบแสดงค่า ที่เราปรับแต่ง req
  res.send("hello world ");
});

app.get("/data/([$])book", function (req, res) {
  console.log(req.url);
  res.send(req.url);
});

app.get("/users/:userId/books/:bookId", function (req, res) {
  res.send(req.params);
});

app.get("/download", function (req, res) {
  res.download("app.js2", "mysavefile.txt", function (err) {
    if (err) {
      //console.log(res.headersSent)
      console.log(err);
      console.log("Can't downlaod");
      res.send("Can't download file!");
    } else {
      console.log("file downloaed");
    }
  });
});

// ส่งกลับข้อความ "hello world" เมื่อมี GET request มายังหน้า homepage
app.get("/", function (req, res) {
  res.send("hello world ");
});
app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});
